﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunSettings : MonoBehaviour
{
    // 6.5 ile -3.7
    public float speed;

    public  Transform targetPos;
    public Transform startPos;

    bool towards = true;


    public void Start()
    {
      
    }

    void Update()
    {

        if (towards)
        {
            transform.position += Vector3.right * speed * Time.deltaTime;
            if ((Mathf.Abs(transform.position.x - startPos.position.x)) < 2.0f)
            {
                towards = false;
                speed = speed + 0.3f;
            }
        }
        else
        {
            transform.position += Vector3.left * speed * Time.deltaTime;
            if ((Mathf.Abs(transform.position.x - targetPos.position.x)) < 2.0f)
            {
                towards = true;
                speed = speed + 0.3f;
            }
        }

        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.position += Vector3.up * speed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.position += Vector3.down * speed * Time.deltaTime;
        }
    }
    
}   


