﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DuckMovement : MonoBehaviour
{
    public float speed = 3f;
   
    void Update()
    {

        transform.Translate(Time.deltaTime * speed, 0, 0);

        if(transform.position.x > 10.5)
        {
            KillTheDuck();
        }

        if (this.gameObject == null)
        {

        }
    }
    public void KillTheDuck()
    {
        Destroy(this.gameObject);
    }
}
