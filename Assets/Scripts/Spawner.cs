﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public Transform duckObjectalfa;
    public Transform duckObjectbeta;

    public Vector3 startPointalfa;
    public Vector3 startPointbeta;

    private Vector3 dir;
    private Vector3 direc;

    int b;
    void Start()
    {
        
        b = 0;

        dir = startPointalfa;
        direc = startPointbeta;

        StartCoroutine(SpawnHandler(dir, direc));
    }
    public void DuckSpawn(Vector3 x, Vector3 y)
   {
    if(b % 2 == 1) 
        { 
          Instantiate(duckObjectalfa, x, Quaternion.identity);
            b++;
        }
        else
        {
          Instantiate(duckObjectbeta, y, Quaternion.identity);
            b++;
        }
   }
    IEnumerator SpawnHandler(Vector3 v,Vector3 c)
    {
        while (true)
        {
            DuckSpawn(v,c);
            yield return new WaitForSeconds(3f);
        }
    }
}

