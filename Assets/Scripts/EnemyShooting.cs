﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShooting : MonoBehaviour
{
    bool isColliding = false;

    
    void Update()
    {
        if (Input.GetKey(KeyCode.E))
        {
            if (isColliding)
            {
                FindObjectOfType<DuckMovement>().KillTheDuck();
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Enemy")
        {
            isColliding = true;

        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Enemy")
        {
            isColliding = false;
        }
    }
}
